#generate self-signed certificate for the CA
openssl req -new -x509 -keyout ca.key -out ca.crt -config openssl.cnf

#Ask the CA to issue a public-key certificate for the server
openssl genrsa -des3 -out server.key 1024

#Generate a Certificate Signing Request(CSR)
openssl req -new -key server.key -out server.csr -config openssl.cnf


