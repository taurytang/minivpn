#generate self-signed certificate for the CA
openssl req -new -x509 -keyout ca_cli.key -out ca_cli.crt -config openssl.cnf

#####################Client side#################
#Ask the CA to issue a public-key certificate for the client
openssl genrsa -des3 -out client.key 1024

#Generate a Certificate Signing Request(CSR)
openssl req -new -key client.key -out client.csr -config openssl.cnf

# Generate acertificate (CRT)
openssl ca -in client.csr -out client.crt -cert ca_cli.crt -keyfile ca_cli.key -config openssl.cnf

